export type TRunnerViews = "live" | "test" | "preview";

export function verifyView(view: string | undefined): TRunnerViews {
    if (view === "test" || view === "preview") {
        return view;
    }

    return "live";
}
