import {
    IDefinition,
    IEpilogue,
    IObservableNode,
    IPrologue,
    ISnapshot,
    Instance,
    L10n,
    NodeBlock,
    Storyline,
    TModes,
    TStatus,
} from "@tripetto/runner";
import { TRunnerViews } from "./views";
import { TRunnerStatus } from "./status";
import { IRunnerSequenceItem } from "./sequence/item";
import { TRunnerPreviewData } from "./preview";

export interface IRunner<
    T extends NodeBlock,
    S = IRunnerSequenceItem<T>[] | Storyline<T> | undefined,
> {
    definition: IDefinition;
    view: TRunnerViews;
    mode: TModes | undefined;
    readonly l10n: L10n.Namespace;
    readonly instance: Instance | undefined;
    readonly fingerprint: string;
    readonly stencil: string;
    readonly actionables: string;
    readonly storyline: S;
    readonly prologue: IPrologue | undefined;
    readonly epilogue: IEpilogue | undefined;
    readonly preview: "prologue" | "blocks" | "epilogue" | undefined;
    readonly status: TRunnerStatus | TStatus;
    readonly isRunning: boolean;
    readonly isFinishing: boolean;
    readonly isEvaluating: boolean;
    readonly isPausing: boolean;
    readonly allowStart: boolean;
    readonly allowRestart: boolean;
    readonly allowPause: boolean;
    readonly allowStop: boolean;
    readonly kickOff: () => void;
    readonly start: () => Instance | undefined;
    readonly restart: () => void;
    readonly reload: (definition: IDefinition) => void;
    readonly pause: <Data>(
        data?: Data,
        pausing?: (
            pSnapshot: ISnapshot<Data>,
            done: (result: "succeeded" | "failed" | "canceled") => void,
            item: IRunnerSequenceItem<T> | undefined
        ) => void
    ) => Promise<ISnapshot<Data>> | ISnapshot<Data> | undefined;
    readonly snapshot: <Data>(data?: Data) => ISnapshot<Data> | undefined;
    readonly stop: () => void;
    readonly update: () => void;
    readonly discard: () => void;
    readonly doAction: (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => void;
    readonly doPreview: (data: TRunnerPreviewData) => void;
    readonly resetPreview: () => void;
}
