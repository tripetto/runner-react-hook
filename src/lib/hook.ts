import {
    Debounce,
    IDefinition,
    IEpilogue,
    IObservableNode,
    IPrologue,
    ISnapshot,
    Instance,
    L10n,
    NodeBlock,
    Runner,
    Storyline,
    TModes,
    arrayItem,
    each,
    findFirst,
    findLast,
    getString,
    isBoolean,
    isFilledString,
    scheduleFrame,
    set,
} from "@tripetto/runner";
import { useEffect, useRef, useState } from "react";
import { TRunnerViews, verifyView } from "./views";
import { TRunnerStatus } from "./status";
import { IRunnerProps } from "./props";
import { IRunner } from "./interface";
import { ISequenceRules } from "./sequence/rules";
import { IRunnerSequenceItem } from "./sequence/item";
import { IPromise } from "./promise";
import { addItemToSequence, findActiveItem } from "./sequence";

export function useRunner<T extends NodeBlock>(
    props: IRunnerProps,
    options?: {
        readonly namespace?: string;
        readonly mode?: TModes;
        readonly bookmarks?: "default" | "group";
        readonly prologue?: "block" | "blend";
        readonly autoStart?: boolean;
        readonly onInteraction?: () => void;
        readonly onPreview?: (
            action: "start" | "end",
            type: "prologue" | "block" | "epilogue",
            blockId?: string
        ) => void;
        readonly onRestart?: () => void;
        readonly onDestroy?: () => void;
    }
): IRunner<T, Storyline<T> | undefined>;
export function useRunner<T extends NodeBlock>(
    props: IRunnerProps,
    options: {
        readonly namespace?: string;
        readonly mode: "sequence";
        readonly prologue?: "block" | "blend";
        readonly autoStart?: boolean;
        readonly restore?: {
            readonly key?: string;
            readonly timeStamp?: number;
        };
        readonly onInteraction?: () => void;
        readonly onPreview?: (
            action: "start" | "end",
            type: "prologue" | "block" | "epilogue",
            blockId?: string
        ) => void;
        readonly onRestart?: () => void;
        readonly onDestroy?: () => void;
    } & ISequenceRules<T>
): IRunner<T, IRunnerSequenceItem<T>[]>;
export function useRunner<T extends NodeBlock>(
    props: IRunnerProps,
    options?: {
        readonly namespace?: string;
        readonly mode?: TModes | "sequence";
        readonly bookmarks?: "default" | "group";
        readonly prologue?: "block" | "blend";
        readonly autoStart?: boolean;
        readonly restore?: {
            readonly key?: string;
            readonly timeStamp?: number;
        };
        readonly onInteraction?: () => void;
        readonly onPreview?: (
            action: "start" | "end",
            type: "prologue" | "block" | "epilogue",
            blockId?: string
        ) => void;
        readonly onRestart?: () => void;
        readonly onDestroy?: () => void;
    } & ISequenceRules<T>
): IRunner<T> {
    const rules = options || {};
    const runnerRef = useRef<Runner<T>>();
    const propsRef = useRef<IRunnerProps>();
    const storylineRef = useRef<Storyline<T>>();
    const sequenceRef = useRef<IRunnerSequenceItem<T>[]>([]);
    const l10nRef = useRef<L10n.Namespace>();
    const viewRef = useRef<TRunnerViews>(verifyView(props.view));
    const testRef = useRef<boolean>(false);
    const promiseRef = useRef<IPromise>();
    const restoreRef = useRef(
        viewRef.current === "live" && props.snapshot && rules.restore
    );
    const [prologuePreview, setProloguePreview] = useState<{
        readonly prologue?: IPrologue;
    }>();
    const [epiloguePreview, setEpiloguePreview] = useState<{
        readonly epilogue?: IEpilogue;
    }>();
    const epilogueRef = useRef<IEpilogue>();
    const [state, setState] = useState<{
        readonly current?: TRunnerStatus;
    }>({});
    const statusRef = useRef<(status: TRunnerStatus | undefined) => void>();
    const setStatus = (status: TRunnerStatus | undefined) =>
        statusRef.current && statusRef.current(status);
    const refs = (sequence: IRunnerSequenceItem<T>[]) => ({
        runnerRef,
        storylineRef,
        viewRef,
        promiseRef,
        sequence,
        rules,
        reset: () => setStatus(undefined),
        restart,
        update,
    });
    let isSynchonizing = false;

    const changeDelegateRef = useRef(
        new Debounce(() => {
            if (
                runnerRef.current &&
                runnerRef.current.instance &&
                propsRef.current &&
                propsRef.current.onChange
            ) {
                propsRef.current.onChange(runnerRef.current.instance);
            }
        })
    );

    const dataDelegateRef = useRef(
        new Debounce(() => {
            if (
                runnerRef.current &&
                runnerRef.current.instance &&
                propsRef.current &&
                propsRef.current.onData
            ) {
                propsRef.current.onData(runnerRef.current.instance);
            }
        })
    );

    const allowStart = () =>
        (runnerRef.current &&
            !runnerRef.current.isRunning &&
            !runnerRef.current.isFinishing &&
            !runnerRef.current.isPausing) ||
        false;

    const allowRestart = () =>
        (viewRef.current !== "preview" &&
            runnerRef.current &&
            !runnerRef.current.isFinishing &&
            !runnerRef.current.isPausing) ||
        false;

    const allowPause = (snapshot: boolean) => {
        if (
            viewRef.current === "live" &&
            runnerRef.current &&
            runnerRef.current.isRunning &&
            storylineRef.current &&
            !storylineRef.current.isEmpty &&
            !storylineRef.current.isEvaluating &&
            !runnerRef.current.isFinishing &&
            !runnerRef.current.isPausing
        ) {
            if (!snapshot && rules.mode === "sequence") {
                const canPauseHere = (item?: IRunnerSequenceItem<T>) =>
                    (item &&
                        (rules.canInteract
                            ? rules.canInteract(item)
                            : item.type === "node") &&
                        item) ||
                    undefined;
                const pausableItem = canPauseHere(
                    findActiveItem(sequenceRef.current)
                );

                if (!pausableItem) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };

    const allowStop = () =>
        (runnerRef.current &&
            runnerRef.current.isRunning &&
            !runnerRef.current.isFinishing &&
            !runnerRef.current.isPausing &&
            (!storylineRef.current || !storylineRef.current.isEmpty)) ||
        false;

    const doAction = (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => {
        if (
            props.onAction &&
            node &&
            runnerRef.current &&
            runnerRef.current.isRunning
        ) {
            props.onAction(
                type,
                {
                    fingerprint: runnerRef.current.fingerprint,
                    name: runnerRef.current.name,
                },
                node && {
                    id: node.key,
                    name: node.props.name || "",
                    get alias() {
                        const alias = getString(node.block?.props, "alias");

                        if (alias) {
                            return alias;
                        }

                        if (node.node.slots) {
                            const slot = findFirst(
                                node.node.slots.all,
                                (slot) =>
                                    slot.kind === "static" && slot.alias
                                        ? true
                                        : false
                            );

                            if (slot) {
                                return slot.alias;
                            }
                        }

                        return undefined;
                    },
                }
            );
        }
    };

    const synchronize = (force: boolean) => {
        isSynchonizing = true;

        if (runnerRef.current) {
            const storyline = runnerRef.current.storyline;

            storylineRef.current = storyline;

            if (rules.mode === "sequence") {
                const sequence = sequenceRef.current;

                if (
                    storyline &&
                    !storyline.isFinishing &&
                    !storyline.isPausing &&
                    (storyline.isChanged || force)
                ) {
                    const prologueItem =
                        sequence.length > 0 &&
                        sequence[0].type === "prologue" &&
                        sequence[0];
                    const prologue = storyline.prologue;

                    if (
                        (prologueItem && !prologue) ||
                        (!prologueItem && prologue)
                    ) {
                        initialize();
                    } else if (prologueItem && prologue) {
                        set(prologueItem, "prologue", prologue);
                    }

                    const nodes = storyline.nodes;
                    const activeItem = findActiveItem(sequence);

                    let index = 0;
                    let active =
                        (activeItem &&
                            ((activeItem.node && activeItem.node.key) ||
                                activeItem.index)) ||
                        (restoreRef.current && restoreRef.current.key) ||
                        -1;
                    let offset = (
                        findFirst(sequence, (item) =>
                            viewRef.current === "preview"
                                ? item.type !== "prologue"
                                : !item.isHistory && item.type === "node"
                        ) || {
                            index: sequence.length,
                        }
                    ).index;

                    for (
                        ;
                        offset < sequence.length && index < nodes.length;
                        offset++, index++
                    ) {
                        const item = sequence[offset];
                        const node = nodes[index];

                        if (item.node && item.node.key === node.key) {
                            set(item, "node", node);

                            if (
                                item.isPreActive ||
                                item.isBeforeActive ||
                                item.isActive ||
                                item.isAfterActive ||
                                item.isPostActive
                            ) {
                                active = offset;
                            }

                            if (
                                viewRef.current !== "preview" &&
                                node.isFailed
                            ) {
                                offset++;
                                index++;

                                break;
                            }
                        } else {
                            break;
                        }
                    }

                    if (offset < sequence.length) {
                        sequence.splice(offset, sequence.length - offset);
                    }

                    for (; index < nodes.length; index++, offset++) {
                        const node = nodes[index];
                        const item = addItemToSequence(
                            refs(sequence),
                            active === node.key ||
                                (active === -1 &&
                                    viewRef.current !== "preview") ||
                                ((isFilledString(active) || active === -1) &&
                                    viewRef.current !== "preview" &&
                                    node.isFailed)
                                ? "active"
                                : isFilledString(active) ||
                                  (active === -1 &&
                                      viewRef.current === "preview")
                                ? "past"
                                : "upcoming",
                            "node",
                            node,
                            doAction
                        );

                        if (item.state === "active") {
                            item.activate(
                                node.key !== restoreRef.current &&
                                    (sequence.length === 1 ||
                                        (item.index > 0 &&
                                            item.index < sequence.length &&
                                            sequence[item.index - 1].isHistory))
                            );

                            active = offset;
                        }
                    }

                    if (viewRef.current !== "preview") {
                        if (
                            storyline.isEmpty &&
                            sequence.length === 1 &&
                            sequence[0].type === "prologue"
                        ) {
                            addItemToSequence(
                                refs(sequence),
                                "upcoming",
                                "epilogue",
                                storyline.epilogue
                            );

                            if (
                                viewRef.current === "live" &&
                                rules.banner &&
                                l10nRef.current
                            ) {
                                const banner = rules.banner(l10nRef.current);

                                if (banner) {
                                    addItemToSequence(
                                        refs(sequence),
                                        "upcoming",
                                        "epilogue",
                                        banner
                                    );
                                }
                            }
                        }

                        if (isFilledString(active) || active === -1) {
                            const firstItem =
                                findFirst(
                                    sequence,
                                    (item) =>
                                        !item.isHistory &&
                                        (!item.node || item.node.isFailed)
                                ) ||
                                findLast(sequence, (item) => !item.isHistory);

                            if (firstItem) {
                                firstItem.activate(true);
                            }
                        }
                    } else {
                        const epilogueItem =
                            sequence.length > 0 &&
                            sequence[sequence.length - 1].type === "epilogue" &&
                            sequence[sequence.length - 1];

                        if (!epilogueItem && sequence.length > 0) {
                            const item = addItemToSequence(
                                refs(sequence),
                                "upcoming",
                                "epilogue",
                                storyline.epilogue
                            );

                            if (activeItem && activeItem.type === "epilogue") {
                                item.activate(false);
                            }
                        } else if (epilogueItem) {
                            set(epilogueItem, "epilogue", storyline.epilogue);
                        }
                    }
                } else if (
                    !storyline &&
                    runnerRef.current.status !== "finished" &&
                    runnerRef.current.status !== "paused"
                ) {
                    const offset = (
                        findFirst(sequence, (item) =>
                            viewRef.current === "test"
                                ? item.type !== "prologue"
                                : !item.isHistory && item.type !== "prologue"
                        ) || {
                            index: sequence.length,
                        }
                    ).index;

                    sequence.splice(offset, sequence.length - offset);
                }

                if (restoreRef.current) {
                    if (restoreRef.current.timeStamp) {
                        const activeItem = findActiveItem(sequence);

                        if (activeItem) {
                            set(
                                activeItem,
                                "timeStamp",
                                restoreRef.current.timeStamp
                            );
                        }
                    }

                    restoreRef.current = undefined;
                }
            }

            if (!storyline && rules.onRestart) {
                rules.onRestart();
            }
        }

        isSynchonizing = false;

        update();
    };

    const update = () => {
        if (isSynchonizing) {
            return;
        }

        if (rules.mode === "sequence" && viewRef.current !== "preview") {
            const activeItem = findActiveItem<T>(sequenceRef.current);

            if (
                activeItem &&
                activeItem.state === "active" &&
                ((activeItem.type === "node" &&
                    rules.canActivate &&
                    rules.canActivate(activeItem)) ||
                    activeItem.type === "epilogue" ||
                    activeItem.type === "paused")
            ) {
                activeItem.next();
            } else if (!activeItem && rules.prologue === "blend") {
                const prologueItem =
                    sequenceRef.current.length === 1 &&
                    sequenceRef.current[0].type === "prologue" &&
                    sequenceRef.current[0];

                if (
                    prologueItem &&
                    prologueItem.isHistory &&
                    prologueItem.prologue &&
                    !prologueItem.prologue.button
                ) {
                    runnerRef.current?.start();
                }
            }
        }

        setState((prev) => ({
            current: prev.current,
        }));

        changeDelegateRef.current.invoke();
    };

    const initialize = () => {
        setStatus(undefined);

        epilogueRef.current = undefined;

        if (rules.mode === "sequence") {
            sequenceRef.current.splice(0, sequenceRef.current.length);

            if (runnerRef.current && runnerRef.current.prologue) {
                addItemToSequence(
                    refs(sequenceRef.current),
                    "history",
                    "prologue",
                    runnerRef.current.prologue
                );
            }
        }
    };

    const start = () => {
        if (allowStart() && runnerRef.current) {
            setStatus(undefined);

            return runnerRef.current.start();
        }

        return undefined;
    };

    const restart = (preserveData: boolean = false) => {
        if (allowRestart() && runnerRef.current) {
            initialize();

            if (
                (!preserveData || !runnerRef.current.isRunning) &&
                runnerRef.current.prologue
            ) {
                runnerRef.current.stop();

                if (rules.mode === "sequence") {
                    const prologueItem =
                        sequenceRef.current.length === 1 &&
                        sequenceRef.current[0].type === "prologue" &&
                        sequenceRef.current[0];

                    if (prologueItem) {
                        prologueItem.activate(!preserveData);
                    }
                } else {
                    update();
                }
            } else {
                runnerRef.current.restart(preserveData);
            }

            if (!preserveData && rules.onRestart) {
                rules.onRestart();
            }
        }
    };

    const reload = (definition: IDefinition) => {
        if (restoreRef.current) {
            restoreRef.current = undefined;
        }

        if (
            runnerRef.current &&
            !runnerRef.current.isFinishing &&
            !runnerRef.current.isPausing
        ) {
            isSynchonizing = runnerRef.current.reload(definition, false)
                ? true
                : false;

            update();
        }
    };

    const pause = <Data>(
        data?: Data,
        on?: (
            snapshot: ISnapshot<Data>,
            done: (
                result: "succeeded" | "failed" | "canceled",
                retry?: () => void
            ) => void,
            item: IRunnerSequenceItem<T> | undefined
        ) => void
    ) => {
        if (!runnerRef.current || !allowPause(false)) {
            return;
        }

        const sequence = rules.mode === "sequence" && sequenceRef.current;

        if (sequence) {
            const activeItem = findActiveItem(sequence);

            if (
                activeItem &&
                activeItem.type === "error" &&
                activeItem.error === "paused"
            ) {
                sequence.splice(
                    activeItem.index,
                    sequence.length - activeItem.index
                );
            } else {
                const canPauseHere = (item?: IRunnerSequenceItem<T>) =>
                    (item &&
                        (rules.canInteract
                            ? rules.canInteract(item)
                            : item.type === "node") &&
                        item) ||
                    undefined;
                const pausableItem = canPauseHere(activeItem);

                if (pausableItem && pausableItem.index + 1 < sequence.length) {
                    sequence.splice(
                        pausableItem.index + 1,
                        sequence.length - pausableItem.index - 1
                    );
                }
            }
        }

        if (on) {
            return runnerRef.current.pause<Data>(
                data,
                (
                    snapshot: ISnapshot<Data>,
                    done: (result: boolean) => void
                ) => {
                    const cancelPause = () => {
                        done(false);

                        setStatus(undefined);

                        if (pauseItem) {
                            pauseItem.undo();

                            synchronize(true);
                        }
                    };

                    const pauseItem =
                        sequence &&
                        addItemToSequence(
                            refs(sequence),
                            "upcoming",
                            "pausing",
                            cancelPause
                        );

                    setStatus("pausing");

                    if (pauseItem) {
                        pauseItem.activate(true);
                    }

                    on!(
                        snapshot,
                        (
                            result: "succeeded" | "failed" | "canceled",
                            retry?: () => void
                        ) => {
                            done(result === "succeeded");

                            setStatus(
                                result === "succeeded"
                                    ? "paused"
                                    : result === "failed"
                                    ? "error-paused"
                                    : undefined
                            );

                            if (pauseItem && result !== "canceled") {
                                if (result === "succeeded") {
                                    pauseItem.changeToPaused();

                                    if (sequence) {
                                        each(
                                            sequence,
                                            (item: IRunnerSequenceItem<T>) => {
                                                if (item.type === "node") {
                                                    item.changeState("history");
                                                }
                                            }
                                        );
                                    }
                                } else {
                                    pauseItem.changeToError(
                                        "paused",
                                        retry &&
                                            (() => {
                                                pauseItem.changeToPausing(
                                                    cancelPause
                                                );

                                                setStatus("pausing");

                                                retry();
                                            }),
                                        cancelPause
                                    );

                                    if (retry) {
                                        return;
                                    }
                                }
                            }

                            if (pauseItem && result === "canceled") {
                                pauseItem.undo();

                                synchronize(true);
                            }
                        },
                        pauseItem || undefined
                    );
                }
            );
        }

        return runnerRef.current.pause(data);
    };

    const autoStart = () => {
        if (runnerRef.current && allowStart()) {
            if (rules.mode === "sequence") {
                const prologueItem = arrayItem(sequenceRef.current, 0);

                if (prologueItem && prologueItem.type === "prologue") {
                    prologueItem.activate(true);

                    return;
                }
            }

            runnerRef.current.start();
        }
    };

    statusRef.current = (current: TRunnerStatus | undefined) => {
        if (state.current !== current) {
            setState({
                current,
            });

            changeDelegateRef.current.invoke();
        }
    };

    if (!l10nRef.current) {
        l10nRef.current =
            props.l10nNamespace || L10n.Namespace.create(rules.namespace || "");
    }

    if (prologuePreview && rules.mode === "sequence") {
        const prologueItem =
            sequenceRef.current.length > 0 &&
            sequenceRef.current[0].type === "prologue" &&
            sequenceRef.current[0];

        if (prologueItem) {
            set(prologueItem, "prologue", prologuePreview.prologue);
        }
    }

    if (epiloguePreview) {
        if (rules.mode === "sequence") {
            const sequence = sequenceRef.current;
            const epilogueItem =
                sequence.length > 0 &&
                sequence[sequence.length - 1].type === "epilogue" &&
                sequence[sequence.length - 1];

            if (
                !epilogueItem &&
                epiloguePreview.epilogue &&
                viewRef.current === "preview"
            ) {
                addItemToSequence(
                    refs(sequence),
                    "upcoming",
                    "epilogue",
                    epiloguePreview.epilogue
                );
            } else if (epilogueItem) {
                set(epilogueItem, "epilogue", epiloguePreview.epilogue);
            }
        }

        if (epilogueRef.current) {
            epilogueRef.current = {
                ...epiloguePreview.epilogue,
                context: epilogueRef.current.context,
            };
        }
    }

    if (!runnerRef.current) {
        runnerRef.current = new Runner({
            namespace: rules.namespace,
            definition: props.definition,
            l10n: l10nRef.current,
            mode:
                rules.mode === "sequence"
                    ? "progressive"
                    : rules.mode || "progressive",
            bookmarks: rules.bookmarks || "default",
            preview: viewRef.current === "preview",
            test: viewRef.current === "test",
            snapshot:
                (viewRef.current === "live" && props.snapshot) || undefined,
            data: props.onImport,
        });

        restoreRef.current =
            (viewRef.current === "live" && rules.restore) || undefined;

        runnerRef.current.onChange = () => synchronize(false);
        runnerRef.current.onData = () => {
            setStatus(undefined);

            dataDelegateRef.current.invoke();

            if (rules.onInteraction) {
                rules.onInteraction();
            }
        };

        runnerRef.current.onFinish = (instance: Instance) =>
            ((viewRef.current !== "live" ||
                props.onSubmit ||
                props.onComplete) &&
                new Promise(
                    (
                        resolve: (reference: string | undefined) => void,
                        reject: (reason?: string) => void
                    ) => {
                        const storyline =
                            runnerRef.current && runnerRef.current.storyline;
                        const sequence =
                            rules.mode === "sequence" && sequenceRef.current;
                        const finishingItem =
                            sequence &&
                            addItemToSequence(
                                refs(sequence),
                                "upcoming",
                                "finishing"
                            );

                        const handleSucces = (reference?: string) => {
                            if (sequence) {
                                each(
                                    sequence,
                                    (item: IRunnerSequenceItem<T>) => {
                                        if (item.type === "node") {
                                            item.changeState("history");
                                        }
                                    }
                                );
                            }

                            resolve(reference);

                            if (props.onComplete) {
                                props.onComplete(instance, reference);
                            }

                            scheduleFrame(() => {
                                epilogueRef.current =
                                    storyline && storyline.epilogue;

                                setStatus("finished");

                                if (
                                    viewRef.current === "live" &&
                                    storyline &&
                                    storyline.epilogue &&
                                    storyline.epilogue.getRedirect
                                ) {
                                    const redirectUrl =
                                        storyline.epilogue.getRedirect();

                                    if (redirectUrl) {
                                        window.location.replace(redirectUrl);

                                        return;
                                    }
                                }

                                if (sequence) {
                                    if (finishingItem) {
                                        finishingItem.changeToEpilogue(
                                            storyline?.epilogue || {}
                                        );
                                    }

                                    if (
                                        viewRef.current === "live" &&
                                        rules.banner &&
                                        l10nRef.current
                                    ) {
                                        const banner = rules.banner(
                                            l10nRef.current
                                        );

                                        if (banner) {
                                            addItemToSequence(
                                                refs(sequence),
                                                "upcoming",
                                                "epilogue",
                                                banner
                                            );
                                        }
                                    }
                                }
                            });
                        };

                        const handleError = (
                            reason?: "outdated" | "rejected" | string
                        ) => {
                            if (finishingItem) {
                                finishingItem.changeToError(
                                    reason === "outdated" ||
                                        reason === "rejected"
                                        ? reason
                                        : "unknown",
                                    (storyline &&
                                        storyline.isFinishable &&
                                        (() => {
                                            if (
                                                storyline &&
                                                storyline.isFinishable
                                            ) {
                                                if (sequence) {
                                                    sequence.splice(
                                                        finishingItem.index,
                                                        sequence.length -
                                                            finishingItem.index
                                                    );
                                                }

                                                storyline.finish();
                                            }
                                        })) ||
                                        undefined
                                );
                            }

                            setStatus(
                                reason === "outdated"
                                    ? "error-outdated"
                                    : reason === "rejected"
                                    ? "error-rejected"
                                    : "error"
                            );

                            if (
                                reason &&
                                reason !== "outdated" &&
                                reason !== "rejected"
                            ) {
                                console.log(reason);
                            }

                            reject(reason);
                        };

                        const result =
                            (viewRef.current === "live" &&
                                props.onSubmit &&
                                props.onSubmit(
                                    instance,
                                    (
                                        props.l10nNamespace ||
                                        L10n.Namespace.global
                                    ).current,
                                    (
                                        props.l10nNamespace ||
                                        L10n.Namespace.global
                                    ).locale.identifier,
                                    rules.namespace
                                )) ||
                            undefined;

                        if (!result || isBoolean(result)) {
                            if (!isBoolean(result) || result) {
                                handleSucces(
                                    (viewRef.current !== "live" && "TEST123") ||
                                        undefined
                                );
                            } else {
                                handleError();
                            }
                        } else {
                            setStatus("finishing");

                            if (finishingItem) {
                                finishingItem.activate(true);
                            }

                            result.then(handleSucces).catch(handleError);
                        }
                    }
                )) ||
            false;

        if (props.onAction) {
            const ref = runnerRef.current;
            const onAction = props.onAction;

            runnerRef.current.hook("OnStart", "synchronous", () => {
                onAction("start", {
                    fingerprint: ref.fingerprint,
                    name: ref.name,
                });
            });

            runnerRef.current.hook("OnFinish", "synchronous", () => {
                onAction("complete", {
                    fingerprint: ref.fingerprint,
                    name: ref.name,
                });
            });

            runnerRef.current.hook("OnPause", "synchronous", () => {
                onAction("pause", {
                    fingerprint: ref.fingerprint,
                    name: ref.name,
                });
            });
        }

        initialize();

        if (
            (typeof rules.autoStart !== "boolean" &&
                (rules.prologue === "blend" || !runnerRef.current.prologue)) ||
            rules.autoStart
        ) {
            autoStart();
        }
    }

    propsRef.current = props;

    useEffect(() => {
        const runnerId = runnerRef.current?.uniqueId || 0;

        if (propsRef.current && propsRef.current.onReady) {
            propsRef.current.onReady(runnerRef.current?.instance);
        }

        return () => {
            if (runnerRef.current?.uniqueId === runnerId) {
                if (rules.mode === "sequence") {
                    sequenceRef.current.splice(0, sequenceRef.current.length);
                }

                runnerRef.current.destroy();

                if (rules.onDestroy) {
                    rules.onDestroy();
                }

                if (propsRef.current && propsRef.current.onDestroy) {
                    propsRef.current.onDestroy();
                }

                runnerRef.current = undefined;
            }
        };
    }, [props.definition, props.snapshot]);

    return {
        l10n: l10nRef.current,
        get definition() {
            return (
                (runnerRef.current && runnerRef.current.definition) ||
                props.definition
            );
        },
        set definition(definition: IDefinition) {
            if (restoreRef.current) {
                restoreRef.current = undefined;
            }

            if (
                runnerRef.current &&
                !runnerRef.current.isFinishing &&
                !runnerRef.current.isPausing
            ) {
                const reloadedInstance = runnerRef.current.reload(
                    definition,
                    viewRef.current !== "preview" && definition.prologue
                        ? true
                        : false
                );
                const instance = reloadedInstance || runnerRef.current.instance;

                isSynchonizing = reloadedInstance ? true : false;

                if (
                    viewRef.current !== "preview" &&
                    !prologuePreview &&
                    !epiloguePreview &&
                    (!instance ||
                        !instance.isRunning ||
                        (sequenceRef.current.length > 0 &&
                            sequenceRef.current[sequenceRef.current.length - 1]
                                .type === "epilogue"))
                ) {
                    isSynchonizing = false;

                    restart(viewRef.current === "test");
                }
            }
        },
        get instance() {
            return runnerRef.current && runnerRef.current.instance;
        },
        get fingerprint() {
            return (runnerRef.current && runnerRef.current.fingerprint) || "";
        },
        get stencil() {
            return (
                (runnerRef.current &&
                    runnerRef.current.stencil("exportables")) ||
                ""
            );
        },
        get actionables() {
            return (
                (runnerRef.current &&
                    runnerRef.current.stencil("actionables")) ||
                ""
            );
        },
        get view() {
            return viewRef.current;
        },
        set view(view: TRunnerViews) {
            if (view !== viewRef.current) {
                if (viewRef.current === "test") {
                    testRef.current =
                        (runnerRef.current && runnerRef.current.isRunning) ||
                        false;
                }

                const preview = view === "preview";
                const test = view === "test";

                viewRef.current = view;

                if (runnerRef.current) {
                    runnerRef.current.isTest = test;

                    if (runnerRef.current.isPreview !== preview) {
                        initialize();

                        runnerRef.current.isPreview = preview;

                        if (
                            view === "preview" ||
                            (view === "test" && testRef.current)
                        ) {
                            return;
                        }
                    }
                }

                restart();
            }
        },
        get mode() {
            return (
                (rules.mode !== "sequence" &&
                    (runnerRef.current?.mode || "progressive")) ||
                undefined
            );
        },
        set mode(mode: TModes | undefined) {
            if (rules.mode !== "sequence" && mode && runnerRef.current) {
                runnerRef.current.mode = mode;
            }
        },
        storyline:
            rules.mode === "sequence"
                ? sequenceRef.current
                : storylineRef.current,
        get prologue() {
            return (viewRef.current === "preview" ||
                (viewRef.current === "test" &&
                    !runnerRef.current?.isRunning &&
                    state.current !== "finished")) &&
                prologuePreview
                ? prologuePreview.prologue
                : runnerRef.current?.prologue;
        },
        get epilogue() {
            return (viewRef.current === "preview" ||
                (viewRef.current === "test" &&
                    !runnerRef.current?.isRunning &&
                    state.current === "finished")) &&
                epiloguePreview
                ? epiloguePreview.epilogue
                : epilogueRef.current;
        },
        get status() {
            return (
                (storylineRef.current &&
                    storylineRef.current.isEvaluating &&
                    "evaluating") ||
                state.current ||
                (runnerRef.current && runnerRef.current.status) ||
                "empty"
            );
        },
        get isRunning() {
            return (runnerRef.current && runnerRef.current.isRunning) || false;
        },
        get isFinishing() {
            return (
                (runnerRef.current && runnerRef.current.isFinishing) || false
            );
        },
        get isPausing() {
            return (runnerRef.current && runnerRef.current.isPausing) || false;
        },
        get isEvaluating() {
            return (
                (storylineRef.current && storylineRef.current.isEvaluating) ||
                false
            );
        },
        get allowStart() {
            return allowStart();
        },
        get allowRestart() {
            return allowRestart();
        },
        get allowPause() {
            return allowPause(false);
        },
        get allowStop() {
            return allowStop();
        },
        kickOff: autoStart,
        start,
        restart,
        reload,
        pause,
        snapshot: <Data>(data?: Data) => {
            if (!runnerRef.current || !allowPause(true)) {
                return;
            }

            return runnerRef.current.snapshot(data);
        },
        stop: () => {
            return allowStop() && runnerRef.current && runnerRef.current.stop();
        },
        update,
        discard: () => {
            if (
                state.current === "error" ||
                state.current === "error-outdated" ||
                state.current === "error-rejected" ||
                state.current === "error-paused"
            ) {
                setStatus(undefined);
            }
        },
        get preview(): "prologue" | "blocks" | "epilogue" | undefined {
            return viewRef.current === "preview"
                ? prologuePreview
                    ? "prologue"
                    : epiloguePreview
                    ? "epilogue"
                    : "blocks"
                : undefined;
        },
        doAction,
        doPreview: (data) => {
            if (data.type === "prologue") {
                setProloguePreview(
                    data.action === "start"
                        ? {
                              prologue: data.ref,
                          }
                        : undefined
                );

                if (data.action === "start" && data.subscribe) {
                    const updatePrologue = new Debounce(
                        (prologue: IPrologue | undefined) =>
                            setProloguePreview({ prologue }),
                        100
                    );

                    data.subscribe((prologue) =>
                        updatePrologue.invoke(prologue)
                    );
                }
            } else if (data.type === "epilogue") {
                setEpiloguePreview(
                    data.action === "start"
                        ? {
                              epilogue: data.ref,
                          }
                        : undefined
                );

                if (data.action === "start" && data.subscribe) {
                    const updateEpilogue = new Debounce(
                        (epilogue: IEpilogue | undefined) =>
                            setEpiloguePreview({ epilogue }),
                        100
                    );

                    data.subscribe((epilogue) =>
                        updateEpilogue.invoke(epilogue)
                    );
                }
            }

            if (viewRef.current === "preview" && rules.onPreview) {
                if (data.type === "prologue") {
                    rules.onPreview(data.action, "prologue", undefined);
                } else if (data.type === "epilogue") {
                    rules.onPreview(data.action, "epilogue", undefined);
                } else if (data.type === "node") {
                    rules.onPreview(data.action, "block", data.ref.id);
                }
            }
        },
        resetPreview: () => {
            if (prologuePreview) {
                setProloguePreview({
                    prologue: undefined,
                });
            }

            if (epiloguePreview) {
                setEpiloguePreview({
                    epilogue: undefined,
                });
            }
        },
    };
}
