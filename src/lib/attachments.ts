export interface IRunnerAttachments {
    /** Fetch an attachment from the store. */
    readonly get: (file: string) => Promise<Blob>;

    /** Put an attachment in the store. */
    readonly put: (
        file: File,
        onProgress?: (percentage: number) => void
    ) => Promise<string>;

    /** Delete an attachment from the store. */
    readonly delete: (file: string) => Promise<void>;
}
