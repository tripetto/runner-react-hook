import { IEpilogue, IPrologue } from "@tripetto/runner";

type TPreviewTypes =
    | "properties"
    | "prologue"
    | "branch"
    | "section"
    | "node"
    | "condition"
    | "epilogue";

interface IPreviewData<Type extends TPreviewTypes, Ref> {
    readonly action: "start" | "end";
    readonly type: Type;
    readonly ref: Ref;
    readonly subscribe?: (onChange: (pRef: Ref) => void) => void;
}

interface IPreviewRef {
    readonly id: string;
}

export type TRunnerPreviewData =
    // eslint-disable-next-line @typescript-eslint/ban-types
    | IPreviewData<"properties", {}>
    | IPreviewData<"prologue", IPrologue | undefined>
    | IPreviewData<"branch", IPreviewRef>
    | IPreviewData<"section", IPreviewRef>
    | IPreviewData<"node", IPreviewRef>
    | IPreviewData<"condition", IPreviewRef>
    | IPreviewData<"epilogue", IEpilogue | undefined>;
