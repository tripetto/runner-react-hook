import { useRef } from "react";
import { L10n, TL10n } from "@tripetto/runner";

export const useL10n = (
    initialL10n: TL10n | undefined,
    onL10n: ((l10n: TL10n) => Promise<void>) | undefined,
    runner: {
        readonly l10n: L10n.Namespace;
        readonly update: () => void;
    },
    purgeCache?: () => void
) => {
    const l10n = useRef<TL10n>(initialL10n || {});
    const updateRef = useRef<() => void>();
    const setL10n = (l: TL10n) => {
        l10n.current = l;

        if (updateRef.current) {
            updateRef.current();
        }
    };

    updateRef.current = () => {
        if (purgeCache) {
            purgeCache();
        }

        runner.update();
    };

    return [
        l10n.current,
        (l: TL10n) => {
            if (onL10n) {
                onL10n(l)
                    .then(() => setL10n(l))
                    .catch(() => setL10n(l));
            } else {
                setL10n(l);
            }
        },
        (language?: string) => {
            if (
                onL10n &&
                ((l10n.current.language !== "auto" && l10n.current.language) ||
                    language ||
                    navigator.language) !== runner.l10n.current
            ) {
                onL10n(l10n.current).then(() => {
                    if (updateRef.current) {
                        updateRef.current();
                    }
                });
            }
        },
    ] as const;
};
