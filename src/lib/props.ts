import { IDefinition, ISnapshot, Instance, L10n } from "@tripetto/runner";
import { TRunnerViews } from "./views";
import { IRunnerAttachments } from "./attachments";

export interface IRunnerProps<Snapshot = unknown> {
    /** Specifies the definition. */
    readonly definition: IDefinition;

    /** Specifies a snapshot that should be restored. */
    readonly snapshot?: ISnapshot<Snapshot>;

    /** Specifies the l10n namespace to use. */
    readonly l10nNamespace?: L10n.Namespace;

    /** Specifies the view mode. */
    readonly view?: TRunnerViews;

    /** Specifies the attachments handler. */
    readonly attachments?: IRunnerAttachments;

    /** Invoked when the runner is ready. */
    readonly onReady?: (instance?: Instance) => void;

    /** Invoked when the runner processed a change. */
    readonly onChange?: (instance: Instance) => void;

    /** Invoked when data can be imported into the instance. */
    readonly onImport?: (instance: Instance) => void;

    /** Invoked when the data in the runner is changed. */
    readonly onData?: (instance: Instance) => void;

    /** Specifies a function that is invoked when the user performs an action. */
    readonly onAction?: (
        type:
            | "start"
            | "stage"
            | "unstage"
            | "focus"
            | "blur"
            | "pause"
            | "complete",
        definition: {
            readonly fingerprint: string;
            readonly name: string;
        },
        block?: {
            readonly id: string;
            readonly name: string;
            readonly alias?: string;
        }
    ) => void;

    /** Invoked when the runner is about to end and submits data. */
    readonly onSubmit?: (
        instance: Instance,
        language: string,
        locale: string,
        namespace: string | undefined
    ) => Promise<string | undefined> | boolean | void;

    /** Invoked when the runner is completed (after the data is submitted). */
    readonly onComplete?: (instance: Instance, id?: string) => void;

    /** Invoked when the runner is destroyed. */
    readonly onDestroy?: () => void;
}
