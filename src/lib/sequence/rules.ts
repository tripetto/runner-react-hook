import { IEpilogue, L10n, NodeBlock } from "@tripetto/runner";
import { IRunnerSequenceItem } from "./item";

export interface ISequenceRules<T extends NodeBlock> {
    /** Verifies if the item can be activated. */
    readonly canActivate?: (item: IRunnerSequenceItem<T>) => boolean;

    /** Verifies if the item is interactive */
    readonly canInteract?: (item: IRunnerSequenceItem<T>) => boolean;

    /** Verifies if an item can be skipped. */
    readonly canSkip?: (item: IRunnerSequenceItem<T>) => boolean;

    /** Retrieves the duration in milliseconds before starting activating the item (this should normally equal the duration of the animation shown in advance of the active state). */
    readonly preActiveDuration?: (
        item: IRunnerSequenceItem<T>
    ) => number | undefined;

    /** Specifies the duration of the state just before it becomes active (this should normally equal the duration of the animation used for showing the active state). */
    readonly beforeActiveDuration?: (
        item: IRunnerSequenceItem<T>
    ) => number | undefined;

    /** Specifies the duration of the state right after losing the active state (this should normally equal the duration of the animation shown when losing the active state). */
    readonly afterActiveDuration?: (
        item: IRunnerSequenceItem<T>
    ) => number | undefined;

    /** Specifies the duration of the state when the active state is lost (this should normally equal the duration of animation shown when the active state is lost). */
    readonly postActiveDuration?: (
        item: IRunnerSequenceItem<T>
    ) => number | undefined;

    /** Invoked when there is interaction between the user and the runner. */
    readonly onInteraction?: () => void;

    /** Returns a banner that is inserted as final epilogue */
    readonly banner?: (l10n: L10n.Namespace) => IEpilogue | undefined;
}
