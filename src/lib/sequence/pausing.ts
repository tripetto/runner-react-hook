export type TPausingRecipes = {
    readonly recipe: "email";
    readonly complete: (emailAddress: string) => void;
    readonly cancel: () => void;
};
