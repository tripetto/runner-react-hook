export type TStates =
    | "history"
    | "past"
    | "pre-active"
    | "before-active"
    | "active"
    | "after-active"
    | "post-active"
    | "upcoming";
