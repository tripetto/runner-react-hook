import {
    IEpilogue,
    IObservableNode,
    IPrologue,
    NodeBlock,
} from "@tripetto/runner";
import { TPausingRecipes } from "./pausing";
import { TStates } from "./states";

export interface IRunnerSequenceItem<T extends NodeBlock> {
    /** Specifies the item type. */
    readonly type:
        | "node"
        | "prologue"
        | "epilogue"
        | "finishing"
        | "pausing"
        | "paused"
        | "error";

    /** Contains a unique id. */
    readonly id: string;

    /** Contains a unique key for the item (this value is only unique within instances). */
    readonly key: string;

    /** Contains a time stamp for the item. */
    readonly timeStamp?: number;

    /** Reference to the node. */
    readonly node?: IObservableNode<T>;

    /** Contains the prologue. */
    readonly prologue?: IPrologue;

    /** Contains the epilogue. */
    readonly epilogue?: IEpilogue;

    /** Contains the error type. */
    readonly error?: "unknown" | "outdated" | "rejected" | "paused";

    /** Specifies the zero based index number of the item. */
    readonly index: number;

    /** Contains the state of the item. */
    readonly state: TStates;

    /** Retrieves if the item is healthy. */
    readonly isHealthy: boolean;

    /** Retrieves if the item is history. */
    readonly isHistory: boolean;

    /** Retrieves if the item is past. */
    readonly isPast: boolean;

    /** Retrieves if the item is pending activation. */
    readonly isPreActive: boolean;

    /** Retrieves if the item is becoming active. */
    readonly isBeforeActive: boolean;

    /** Retrieves if the item is active. */
    readonly isActive: boolean;

    /** Retrieves if the item is losing activation. */
    readonly isAfterActive: boolean;

    /** Retrieves if the item was active. */
    readonly isPostActive: boolean;

    /** Retrieves if the item is upcoming. */
    readonly isUpcoming: boolean;

    /** Retrieves if the item is the last item. */
    readonly isFirst: boolean;

    /** Retrieves if the item is the first item. */
    readonly isLast: boolean;

    /** Retrieves if the item is paused. */
    readonly isPaused: boolean;

    /** Activates the item. */
    readonly activate: (useTransition?: boolean) => boolean;

    /** Deactivates the item. */
    readonly deactivate: () => boolean;

    /** Retrieves if the item can be activated. */
    readonly allowActivate: boolean;

    /** Retrieves if a step to the next item can be made. */
    readonly allowNext: boolean;

    /** Retrieves if an item can undo. */
    readonly allowUndo: boolean;

    /** Retrieves if an item can be skipped. */
    readonly allowSkip: boolean;

    /** Steps to the next item. */
    readonly next: () => boolean;

    /** Steps to the previous item. */
    readonly undo: () => boolean;

    /** Skip the current item. */
    readonly skip: () => boolean;

    /** Waits before activating or deactivating an item. */
    readonly wait: () => void;

    /** Continues with the activating or deactivating the item. */
    readonly continue: () => void;

    /** Repeats a sequence. */
    readonly repeat?: () => void;

    /** Retrieves if the kick-off function. */
    readonly kickOff?: () => void;

    /** Retries an action after an error. */
    readonly retry?: () => void;

    /** Cancels the action after an error. */
    readonly cancel?: () => void;

    /** Changes the state of an item. */
    readonly changeState: (state: TStates) => void;

    /** Change to the item to an error. */
    readonly changeToError: (
        errorType: "unknown" | "outdated" | "rejected" | "paused",
        retry?: () => void,
        cancel?: () => void
    ) => void;

    /** Change to the item to an epilogue. */
    readonly changeToEpilogue: (epilogue: IEpilogue) => void;

    /** Change to the item to a pausing type. */
    readonly changeToPausing: (cancel: () => void) => void;

    /** Change to the item to a paused type. */
    readonly changeToPaused: () => void;

    /** Contains the pausing recipe. */
    readonly pausingRecipe?: TPausingRecipes;

    /** Sets the pausing recipe. */
    readonly setPausingRecipe: (recipe: TPausingRecipes) => void;
}
