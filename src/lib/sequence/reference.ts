import { NodeBlock, Runner, Storyline } from "@tripetto/runner";
import { MutableRefObject } from "react";
import { TRunnerViews } from "../views";
import { IRunnerSequenceItem } from "./item";
import { ISequenceRules } from "./rules";
import { IPromise } from "../promise";

export interface IRef<T extends NodeBlock> {
    readonly runnerRef: MutableRefObject<Runner<T> | undefined>;
    readonly storylineRef: MutableRefObject<Storyline<T> | undefined>;
    readonly viewRef: MutableRefObject<TRunnerViews>;
    readonly promiseRef: MutableRefObject<IPromise | undefined>;
    readonly sequence: IRunnerSequenceItem<T>[];
    readonly rules: ISequenceRules<T>;
    readonly reset: () => void;
    readonly restart: () => void;
    readonly update: () => void;
}
