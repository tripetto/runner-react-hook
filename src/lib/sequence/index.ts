import {
    DateTime,
    IEpilogue,
    IObservableNode,
    IPrologue,
    NodeBlock,
    Num,
    SHA2,
    arrayItem,
    findLast,
    set,
} from "@tripetto/runner";
import { IRunnerSequenceItem } from "./item";
import { IRef } from "./reference";
import { TStates } from "./states";
import { promise } from "../promise";

/** Item enumerator */
let enumerator = 0;

/**
 * Finds the active item.
 * @param sequence Reference to the item sequence.
 * @return Returns a reference to the item or `undefined` if there is no active item.
 */
export function findActiveItem<T extends NodeBlock>(
    sequence: IRunnerSequenceItem<T>[]
): IRunnerSequenceItem<T> | undefined {
    return findLast(
        sequence,
        (item) =>
            item.isPreActive ||
            item.isBeforeActive ||
            item.isActive ||
            item.isAfterActive ||
            item.isPostActive
    );
}

/**
 * Finds the previous item.
 * @param sequence Reference to the item sequence.
 * @param index Specifies the item index to start.
 * @param validate Specifies a validation function to validate the item.
 * @return Returns a reference to the item or `undefined` if no validated previous item was found.
 */
function findPreviousItem<T extends NodeBlock>(
    sequence: IRunnerSequenceItem<T>[],
    index: number,
    validate?: (item: IRunnerSequenceItem<T>) => boolean
): IRunnerSequenceItem<T> | undefined {
    const currentItem = arrayItem(sequence, index);

    if (currentItem && currentItem.isHealthy) {
        let previous = currentItem.index;

        while (--previous >= 0) {
            const item = sequence[previous];

            if (item.isHistory) {
                break;
            }

            if (!validate || validate(item)) {
                return item;
            }
        }
    }

    return undefined;
}

/**
 * Activates an item.
 * @param sequence Reference to the item sequence.
 * @param index Specifies the item index to activate.
 * @param bMakeHistory Specifies if past items needs to be marked as history.
 * @param doAction Reference to the action function.
 * @return Returns `true` if the item is activated.
 */
function activateItem<T extends NodeBlock>(
    sequence: IRunnerSequenceItem<T>[],
    index: number,
    isPreview: boolean,
    doAction?: (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => void
): boolean {
    if (
        sequence.length === 0 ||
        index < 0 ||
        index >= sequence.length ||
        !sequence[index].isHealthy ||
        (!isPreview &&
            sequence[index].isHistory &&
            sequence[index].type !== "prologue")
    ) {
        return false;
    }

    const activeItem = findActiveItem(sequence);
    const currentIndex = (activeItem || { index: -1 }).index;

    if (currentIndex !== index) {
        const from = currentIndex === -1 ? 0 : Num.min(index, currentIndex);
        const to =
            currentIndex === -1
                ? sequence.length - 1
                : Num.max(index, currentIndex);
        let activate: IRunnerSequenceItem<T> | undefined;

        for (let i = from; i <= to; i++) {
            const item = sequence[i];

            if (!item.isHistory || item.type === "prologue") {
                item.changeState(
                    i === index
                        ? "active"
                        : i < index
                        ? item.type !== "node"
                            ? "history"
                            : "past"
                        : "upcoming"
                );

                if (!activate && i === index) {
                    activate = item;
                }

                if (
                    !isPreview &&
                    i > index &&
                    item.type !== "node" &&
                    item.type !== "epilogue"
                ) {
                    sequence.splice(i, sequence.length - i);

                    break;
                }
            }
        }

        if (doAction) {
            if (activeItem && !activeItem.isActive) {
                doAction("unstage", activeItem.node);
            }

            if (activate) {
                doAction("stage", activate.node);
            }
        }
    }

    return true;
}

/**
 * Deactivates an item.
 * @param sequence Reference to the item sequence.
 * @param index Specifies the item index to deactivate.
 * @param doAction Reference to the action function.
 * @return Returns `true` if the item is deactivated.
 */
function deactivateItem<T extends NodeBlock>(
    sequence: IRunnerSequenceItem<T>[],
    index: number,
    doAction?: (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => void
): boolean {
    if (
        index < 0 ||
        index >= sequence.length ||
        !sequence[index].isHealthy ||
        !sequence[index].isActive
    ) {
        return false;
    }

    if (doAction) {
        const current = sequence[index];

        if (current?.isActive) {
            doAction("unstage", current.node);
        }
    }

    while (index < sequence.length) {
        const item = sequence[index];

        if (!item.isHistory && !item.isAfterActive && !item.isPostActive) {
            item.changeState(item.type !== "node" ? "history" : "past");
        }

        index++;
    }

    return true;
}

export function addItemToSequence<T extends NodeBlock>(
    refs: IRef<T>,
    state: "past" | "active" | "upcoming",
    type: "node",
    node: IObservableNode<T>,
    doAction?: (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => void
): IRunnerSequenceItem<T>;

export function addItemToSequence<T extends NodeBlock>(
    refs: IRef<T>,
    state: "history",
    type: "prologue",
    prologue: IPrologue
): IRunnerSequenceItem<T>;

export function addItemToSequence<T extends NodeBlock>(
    refs: IRef<T>,
    state: "upcoming",
    type: "epilogue",
    epilogue: IEpilogue
): IRunnerSequenceItem<T>;

export function addItemToSequence<T extends NodeBlock>(
    refs: IRef<T>,
    state: "upcoming",
    type: "finishing" | "pausing",
    cancel?: () => void
): IRunnerSequenceItem<T>;

export function addItemToSequence<T extends NodeBlock>(
    refs: IRef<T>,
    state: "history" | "past" | "active" | "upcoming",
    type: "node" | "prologue" | "epilogue" | "finishing" | "pausing",
    data?: IObservableNode<T> | IEpilogue | IPrologue | (() => void),
    doAction?: (
        type: "stage" | "unstage" | "focus" | "blur",
        node?: IObservableNode<NodeBlock>
    ) => void
): IRunnerSequenceItem<T> {
    const node = (type === "node" && (data as IObservableNode<T>)) || undefined;
    const prologue = (type === "prologue" && (data as IPrologue)) || undefined;
    const epilogue = (type === "epilogue" && (data as IEpilogue)) || undefined;
    const cancel =
        ((type === "finishing" || type === "pausing") &&
            (data as () => void)) ||
        undefined;
    const n = enumerator++;
    const item: IRunnerSequenceItem<T> = {
        id:
            "_" +
            SHA2.CSHA2_256(
                node
                    ? node.key +
                          (refs.viewRef.current === "live" &&
                              refs.storylineRef.current &&
                              refs.storylineRef.current.instance.id)
                    : `${type}-${refs.sequence.length}`
            ),
        key:
            (node && node.key) ||
            "_" + SHA2.CSHA2_256(`${type}-${refs.sequence.length}`),
        type,
        node,
        prologue,
        epilogue,
        cancel,
        index: refs.sequence.length,
        state,
        changeState: (newState: TStates) => {
            set(
                item,
                "state",
                item.type === "prologue" &&
                    refs.viewRef.current !== "preview" &&
                    newState === "active"
                    ? "history"
                    : newState
            );

            if (
                item.state === "history" &&
                refs.viewRef.current === "live" &&
                node
            ) {
                set(item, "key", "_" + SHA2.CSHA2_256(`history-${n}`));
            }

            if (node && item.state === "upcoming") {
                set(item, "timeStamp", undefined);
            }
        },
        get isHealthy() {
            const currentItem = arrayItem(refs.sequence, item.index);

            return (currentItem && currentItem.id === item.id) || false;
        },
        get isHistory() {
            return item.state === "history";
        },
        get isPast() {
            return item.state === "past";
        },
        get isPreActive() {
            return item.state === "pre-active";
        },
        get isBeforeActive() {
            return item.state === "before-active";
        },
        get isActive() {
            return item.state === "active";
        },
        get isAfterActive() {
            return item.state === "after-active";
        },
        get isPostActive() {
            return item.state === "post-active";
        },
        get isUpcoming() {
            return item.state === "upcoming";
        },
        get isFirst() {
            return item.index === 0;
        },
        get isLast() {
            return item.index === refs.sequence.length - 1;
        },
        get isPaused() {
            if (item.index + 1 < refs.sequence.length) {
                const nextItem = refs.sequence[item.index + 1];

                if (
                    nextItem.type === "pausing" ||
                    nextItem.type === "paused" ||
                    (nextItem.type === "error" && nextItem.error === "paused")
                ) {
                    return true;
                }
            }

            return false;
        },
        get allowActivate() {
            return item.isHealthy &&
                !item.isHistory &&
                node &&
                refs.storylineRef.current &&
                !refs.storylineRef.current.isFinishing &&
                !refs.storylineRef.current.isPausing
                ? true
                : false;
        },
        activate: (useTransition = false) => {
            if (
                node &&
                refs.storylineRef.current &&
                refs.storylineRef.current.isPausing
            ) {
                const activeItem = findActiveItem(refs.sequence);

                if (activeItem && activeItem.cancel) {
                    activeItem.cancel();
                }
            }

            if (
                node &&
                (!refs.storylineRef.current ||
                    refs.storylineRef.current.isFinishing ||
                    refs.storylineRef.current.isPausing)
            ) {
                return false;
            }

            promise(refs.promiseRef).reject();

            if (
                activateItem(
                    refs.sequence,
                    item.index,
                    refs.viewRef.current === "preview",
                    doAction
                )
            ) {
                const makeActive = () => {
                    item.changeState("active");
                    refs.update();

                    if (refs.rules.onInteraction) {
                        refs.rules.onInteraction();
                    }
                };

                const beforeActive = () => {
                    if (
                        useTransition &&
                        refs.viewRef.current !== "preview" &&
                        refs.rules.beforeActiveDuration
                    ) {
                        const beforeActiveDuration =
                            refs.rules.beforeActiveDuration(item) || 0;

                        if (beforeActiveDuration) {
                            refs.promiseRef.current = {
                                resolve: () =>
                                    item.state === "before-active" &&
                                    makeActive(),
                                reject: () => item.changeState("upcoming"),
                                handle: setTimeout(
                                    () => item.continue(),
                                    beforeActiveDuration
                                ),
                            };

                            item.changeState("before-active");
                            refs.update();
                        } else {
                            makeActive();
                        }
                    } else {
                        makeActive();
                    }
                };

                if (item.type === "node") {
                    refs.reset();
                }

                if (
                    useTransition &&
                    refs.viewRef.current !== "preview" &&
                    refs.rules.preActiveDuration
                ) {
                    const preActiveDuration =
                        refs.rules.preActiveDuration(item) || 0;

                    if (preActiveDuration > 0) {
                        refs.promiseRef.current = {
                            resolve: () =>
                                item.state === "pre-active" && beforeActive(),
                            reject: () => item.changeState("upcoming"),
                            handle: setTimeout(
                                () => item.continue(),
                                preActiveDuration
                            ),
                        };

                        item.changeState("pre-active");
                        refs.update();
                    } else {
                        beforeActive();
                    }
                } else {
                    beforeActive();
                }

                if (refs.rules.onInteraction) {
                    refs.rules.onInteraction();
                }

                return true;
            }

            return false;
        },
        deactivate: () => {
            if (
                refs.viewRef.current === "preview" &&
                deactivateItem(refs.sequence, item.index, doAction)
            ) {
                refs.update();

                return true;
            }

            return false;
        },
        get allowNext() {
            return (
                (!item.isHistory &&
                    refs.storylineRef.current &&
                    !refs.storylineRef.current.isEvaluating &&
                    !refs.storylineRef.current.isFinishing &&
                    !refs.storylineRef.current.isPausing &&
                    (!item.node || item.node.isPassed) &&
                    item.isHealthy &&
                    (refs.viewRef.current === "preview" ||
                        item.index + 1 < refs.sequence.length ||
                        refs.storylineRef.current.isFinishable ||
                        false)) ||
                false
            );
        },
        next: () => {
            if (!item.isHealthy) {
                return false;
            }

            if (refs.viewRef.current !== "preview") {
                const lock = () => {
                    if (item.node && item.node.block) {
                        item.node.block.lock();
                    }
                };

                const unlock = () => {
                    if (item.node && item.node.block) {
                        item.node.block.unlock();
                    }

                    return true;
                };

                if (refs.rules.afterActiveDuration && item.isActive) {
                    const duration = refs.rules.afterActiveDuration(item) || 0;

                    if (duration > 0) {
                        refs.promiseRef.current = {
                            resolve: () =>
                                item.state === "after-active" && item.next(),
                            reject: () =>
                                unlock() &&
                                item.changeState(
                                    item.type !== "node" ? "history" : "past"
                                ),
                            handle: setTimeout(() => item.continue(), duration),
                        };

                        lock();

                        item.changeState("after-active");
                        refs.update();

                        return true;
                    }
                }

                if (
                    refs.rules.postActiveDuration &&
                    (item.isActive || item.isAfterActive)
                ) {
                    const duration = refs.rules.postActiveDuration(item) || 0;

                    if (duration > 0) {
                        refs.promiseRef.current = {
                            resolve: () =>
                                item.state === "post-active" && item.next(),
                            reject: () =>
                                unlock() &&
                                item.changeState(
                                    item.type !== "node" ? "history" : "past"
                                ),
                            handle: setTimeout(() => item.continue(), duration),
                        };

                        lock();

                        item.changeState("post-active");
                        refs.update();

                        return true;
                    }
                }

                unlock();
            } else if (item.deactivate()) {
                return true;
            }

            if ((item.node && !item.node.isPassed) || item.isHistory) {
                return false;
            }

            if (item.index + 1 < refs.sequence.length) {
                return refs.sequence[item.index + 1].activate(true);
            }

            if (item.type !== "node") {
                item.changeState("history");

                return false;
            }

            item.changeState("past");

            return (
                (refs.storylineRef.current &&
                    refs.storylineRef.current.stepForward()) ||
                false
            );
        },
        get allowUndo() {
            return (
                (refs.storylineRef.current &&
                    !refs.storylineRef.current.isFinishing &&
                    !refs.storylineRef.current.isEvaluating &&
                    ((item.type === "node" &&
                        !refs.storylineRef.current.isPausing) ||
                        (item.type === "pausing" && item.pausingRecipe)) &&
                    findPreviousItem(
                        refs.sequence,
                        item.index,
                        refs.rules.canInteract
                    ) &&
                    true) ||
                false
            );
        },
        undo: () => {
            if (
                refs.storylineRef.current &&
                !refs.storylineRef.current.isEvaluating
            ) {
                if (item.type === "pausing" && item.pausingRecipe) {
                    item.pausingRecipe.cancel();

                    return true;
                }

                const previousItem = findPreviousItem(
                    refs.sequence,
                    item.index,
                    refs.rules.canInteract
                );

                return (previousItem && previousItem.activate(false)) || false;
            }

            return false;
        },
        get allowSkip() {
            return (
                (refs.storylineRef.current &&
                    !refs.storylineRef.current.isFinishing &&
                    !refs.storylineRef.current.isPausing &&
                    !refs.storylineRef.current.isEvaluating &&
                    item.type === "node" &&
                    refs.rules.canSkip &&
                    refs.rules.canSkip(item)) ||
                false
            );
        },
        skip: () => {
            if (item.isHealthy && item.allowSkip) {
                if (item.node && item.node.block) {
                    item.node.block.clear();
                }

                return item.next();
            }

            return false;
        },

        wait: () => promise(refs.promiseRef).cancel(),
        continue: () => promise(refs.promiseRef).resolve(),
        get repeat() {
            if (
                item.type === "epilogue" &&
                item.epilogue &&
                (refs.viewRef.current === "test" ||
                    (refs.viewRef.current === "live" &&
                        item.epilogue.repeatable))
            ) {
                for (
                    let index = item.index + 1;
                    index < refs.sequence.length;
                    index++
                ) {
                    if (refs.sequence[index].type !== "epilogue") {
                        return undefined;
                    }
                }

                return () => {
                    if (refs.viewRef.current === "test") {
                        refs.restart();
                    } else if (
                        refs.runnerRef.current &&
                        !refs.runnerRef.current.isRunning
                    ) {
                        refs.runnerRef.current.start();
                    }
                };
            }

            return undefined;
        },
        get kickOff() {
            if (
                refs.viewRef.current !== "preview" &&
                item.type === "prologue" &&
                refs.runnerRef.current &&
                !refs.runnerRef.current.isRunning &&
                !refs.runnerRef.current.isFinishing &&
                !refs.runnerRef.current.isPausing &&
                item.isLast
            ) {
                return () =>
                    refs.runnerRef.current && refs.runnerRef.current.start();
            }

            return undefined;
        },
        changeToError: (
            errorType: "unknown" | "outdated" | "rejected" | "paused",
            retry?: () => void,
            cancelPausing?: () => void
        ) => {
            set(item, "type", "error");
            set(item, "error", errorType);
            set(item, "retry", retry);
            set(item, "cancel", cancelPausing);
            set(item, "timeStamp", DateTime.now);

            if (!item.activate(true)) {
                refs.update();
            }
        },
        changeToEpilogue: (epilogueRef) => {
            set(item, "type", "epilogue");
            set(item, "epilogue", epilogueRef);
            set(item, "timeStamp", DateTime.now);

            if (!item.activate(true)) {
                refs.update();
            }
        },
        changeToPausing: (cancelPausing?: () => void) => {
            set(item, "type", "pausing");
            set(item, "retry", undefined);
            set(item, "cancel", cancelPausing);
            set(item, "timeStamp", undefined);

            if (!item.activate(true)) {
                refs.update();
            }
        },
        changeToPaused: () => {
            set(item, "type", "paused");
            set(item, "retry", undefined);
            set(item, "cancel", undefined);
            set(item, "timeStamp", DateTime.now);

            if (!item.activate(true)) {
                refs.update();
            }
        },
        pausingRecipe: undefined,
        setPausingRecipe: (recipeDescriptor) => {
            switch (recipeDescriptor.recipe) {
                case "email":
                    if (item.type === "pausing") {
                        const reset = () => {
                            set(item, "pausingRecipe", undefined);
                            set(item, "cancel", undefined);

                            refs.update();
                        };

                        set(item, "pausingRecipe", {
                            recipe: recipeDescriptor.recipe,
                            complete: (emailAddress: string) => {
                                reset();
                                recipeDescriptor.complete(emailAddress);
                            },
                            cancel: () => {
                                reset();
                                recipeDescriptor.cancel();
                            },
                        });

                        refs.update();
                    }
                    break;
            }
        },
    };

    refs.sequence.push(item);

    if (doAction && state === "active") {
        doAction("stage", item.node);
    }

    return item;
}
