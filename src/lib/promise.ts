import { MutableRefObject } from "react";

export interface IPromise {
    readonly resolve: () => void;
    readonly reject: () => void;
    handle: number;
}

export const promise = (ref: MutableRefObject<IPromise | undefined>) => {
    const promiseRef = ref.current;
    const cancel = () => {
        if (promiseRef && promiseRef.handle !== 0) {
            clearTimeout(promiseRef.handle);

            promiseRef.handle = 0;
        }
    };

    return {
        resolve: () => {
            ref.current = undefined;

            if (promiseRef) {
                cancel();

                promiseRef.resolve();
            }
        },
        reject: () => {
            ref.current = undefined;

            if (promiseRef) {
                cancel();

                promiseRef.reject();
            }
        },
        cancel,
    };
};
