export type TRunnerStatus =
    | "evaluating"
    | "finishing"
    | "finished"
    | "pausing"
    | "paused"
    | "error"
    | "error-outdated"
    | "error-rejected"
    | "error-paused";
