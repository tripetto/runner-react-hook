## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 📦 Tripetto Runner React Hook
[![Version](https://badgen.net/npm/v/@tripetto/runner-react-hook?icon=npm&label)](https://www.npmjs.com/package/@tripetto/runner-react-hook)
[![Downloads](https://badgen.net/npm/dt/@tripetto/runner-react-hook?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/runner-react-hook)
[![License](https://badgen.net/npm/license/@tripetto/runner-react-hook?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/runner-react-hook)
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/runner/api/react-hook/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/runner-react-hook/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

This package wraps the functionality of the [Tripetto Runner](https://tripetto.com/sdk/docs/runner/api/library/) into an easy to use [React Hook](https://reactjs.org/docs/hooks-intro.html). It greatly simplifies the process of building a runner for Tripetto using [React](https://reactjs.org/). All of Tripetto's [stock runners](https://tripetto.com/sdk/docs/runner/stock/introduction/) are built using this hook.

## 🚀 Get started
You can find all the information to start with this package at [tripetto.com/sdk/docs/runner/api/react-hook/](https://tripetto.com/sdk/docs/runner/api/react-hook/).

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 🆘 Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/runner-react-hook/-/issues).

Need help or assistance? Please go to our [support page](https://tripetto.com/sdk/support/). We're more than happy to help you.

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
